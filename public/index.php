<?php
/**
 * Creator:  Denis Golikov
 * Date: 02.03.2019
 * Time: 23:00
 */

use Core\Kernel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


$bootstrap = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap'.DIRECTORY_SEPARATOR;
require_once $bootstrap.'autoload.php';


if (DEV_MODE) {
    require_once ROOT.'bin'.DIRECTORY_SEPARATOR.'assetsBypass.php';
}

$request = Request::createFromGlobals();
$app = new Kernel();

try {
    $response = $app->execute($request);
} catch (\Exception | Error | Throwable $e) {
    $response = new Response('Internal server Error', 500);
}

$response->send();

exit();