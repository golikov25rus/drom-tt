/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App
 * @class TodoItem
 */
export default class TodoItem {
    /**
     * @access public
     * @param {string} hash
     * @param {int} [id = 0]
     * @param {int} todoId
     * @param {int} stateId
     * @param {string} stateName
     * @param {string} content
     */
    constructor({hash, id = 0, todoId, stateId = 1, stateName = 'active', content}) {
        /**
         * @access public
         * @type {string}
         */
        this.hash = hash;
        /**
         * @access public
         * @type {int}
         */
        this.id = id;

        /**
         * @access public
         * @type {int}
         */
        this.todoId = todoId;

        /**
         * @access public
         * @type {int}
         */
        this.stateId = stateId;

        /**
         * @access public
         * @type {string}
         */
        this.stateName = stateName;

        /**
         * @access public
         * @type {string}
         * @desc
         * Первоначальный контент, с которым создается модель
         * После привязки к HTMLElement контентом считается value HTMLElement
         */
        this.internalContent = content.replace(/^[\s]+|[\s]+$/g, '');

        /**
         * @access public
         * @type {Element}
         */
        this.wrapperElement = undefined;

        /**
         * @access public
         * @type {Element}
         */
        this.inputElement = undefined;

        /**
         * @access public
         * @type {Element}
         */
        this.contentElement = undefined;

        /**
         * @access public
         * @type {Element}
         */
        this.checkboxElement = undefined;

        /**
         * @access public
         * @type {Element}
         */
        this.deleteButtonElement = undefined;
    }

    /**
     * @access public
     * @return {string}
     * @desc
     * Если HTMLElement не связан с моделью, отдает контент, с которым модель была создана
     */
    get content() {
        return typeof this.contentElement === 'undefined' ? this.internalContent : this.contentElement.textContent ;
    }

    /**
     * @access public
     * @param {string} value
     * @return void
     * @desc
     * При установке контента обрезает пробелы по краям, а так же убирает врапперу класс
     * что бы скрыть инпут
     */
    set content(value) {
        value = value.replace(/^[\s]+|[\s]+$/g, '');
        this.contentElement.textContent = this.internalContent = value;
        this.wrapperElement.classList.remove('editing');
    }

    /**
     * @access public
     * @return {string}
     */
    get input() {
        return this.inputElement.value;
    }

    /**
     * @access public
     * @param {string} value
     * @return void
     * При установке контента инпуту модели, она добавляет врапперу класс, что бы инпут был видим
     * и фокусирует на инпуте курсор
     */
    set input(value) {
        this.inputElement.value = value;
        this.wrapperElement.classList.add('editing');
        this.inputElement.focus();
    }

    /**
     * @access public
     * @param {Element} wrapper
     * @return void
     * При присваивании враппера модель мапит его содержимое на отдельные Element-ы,
     * необходимые в дальнейшем
     */
    set element(wrapper) {
        let input = wrapper.lastElementChild,
            checkbox = wrapper.firstElementChild.children[0],
            content = wrapper.firstElementChild.children[1],
            deleteButton = wrapper.firstElementChild.children[2];
        this.wrapperElement = wrapper;
        this.inputElement = input;
        this.contentElement = content;
        this.deleteButtonElement = deleteButton;
        this.checkboxElement = checkbox
    }

    /**
     * @access public
     * @param {int} value
     * @return void
     * При присвоении статуса так же переключает классы враппера в зависимости от переданного значения
     */
    set state(value) {
        this.checkboxElement.checked = value !== 1;
        this.stateId = value;
        if(value === 1) {
            this.wrapperElement.classList.remove('completed');
            this.wrapperElement.classList.add('open');
        } else {
            this.wrapperElement.classList.remove('open');
            this.wrapperElement.classList.add('completed');
        }
    }

    /**
     * @access public
     * @return void
     * Переключает статус
     */
    toggleState() {
        if(this.stateId === 1) {
            this.stateId = 2;
            this.stateName = 'completed';
            this.wrapperElement.classList.remove('open');
            this.wrapperElement.classList.add('completed');
        } else {
            this.stateId = 1;
            this.stateName = 'open';
            this.wrapperElement.classList.remove('completed');
            this.wrapperElement.classList.add('open');
        }
    }

    /**
     * @access public
     * @return void
     * Удаляет Element враппера и его вложения.
     * Используется перед уничтожением самой модели
     */
    selfElementDelete() {
        this.wrapperElement.remove();
    }
}