/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App
 * @class TodoRepository
 * @desc
 * Класс для работы с состоянием TodoItem-ов
 */
export default class TodoRepository {
    /**
     * @access public
     * @param {TodoItem} model - класс модели
     * @param {string} endpoint - restApi endpoint
     * @param {int} pollingTimeout - интервал проверки ChangeLog в милисекундах
     */
    constructor({model, endpoint, pollingTimeout}) {
        /**
         * @access private
         * @type {TodoItem}
         */
        this.model = model;

        /**
         * @access private
         * @type {string}
         */
        this.endpoint = endpoint;

        /**
         * @access private
         * @type {int}
         */
        this.timeout = pollingTimeout;

        /**
         * @access public
         * @type {Object}
         */
        this.chagesLog = { count: 0, log: {}};

        /**
         * @access public
         * @type {Object}
         * @desc
         * ChangeLog, который находится в отправке
         */
        this.currentRequest = { count: 0, log: {}};

        /**
         * @access public
         * @type {string}
         * @desc
         * ChangeLog, который находится в отправке в строковом эквиваленте
         */
        this.requestData = undefined;

        /**
         * @access public
         * @callback
         * @type {Function}
         * @desc
         * Коллбек, вызываемый после получения ответа от сервера
         */
        this.todoItemsIdUpdateCallback = undefined;

        this.initDelaySynchronization(pollingTimeout);
    }

    /**
     * @access private
     * @return void
     * @desc
     * Инициализация проверки лога с временной задержкой
     */
    initDelaySynchronization(timeout) {
        setTimeout(() => {
            this.synchronizeChanges();
        }, timeout)
    }

    /**
     * @access public
     * @param {string} content
     * @param {int} todoId
     * @return {TodoItem}
     * @desc
     * Создание экземпляра модели TodoItem
     */
    createNewTodoItem(content, todoId) {
        let hash = this.getHash();
        return new this.model({hash, content, todoId})
    }

    /**
     * @access public
     * @param {Element} wrapper
     * @return {{TodoItem}}
     * @desc
     * Парсит враппер todoItem-ов и создает из них коллекцию моделей
     */
    getItemsFromWrapper(wrapper) {
        let result = {};
        for(let child of wrapper.children) {
            let data = child.dataset.item.split('-'),
                hash = this.getHash(),
                id = parseInt(data[0]),
                todoId = parseInt(data[1]),
                stateId = parseInt(data[2]),
                stateName = data[3],
                content = child.firstElementChild.children[1].textContent;
            result[hash] = new this.model({hash, id, todoId, stateId, stateName, content});
        }
        return result;
    }

    /**
     * @access public
     * @return {string}
     * @desc
     * Генерирует хеш
     */
    getHash() {
        return `${(~~(Math.random()*1e8)).toString(16)}`;
    }

    /**
     * @access public
     * @param {TodoItem} todoItem
     * @param {int} stateType
     * @return void
     * @desc
     * Сохраняет изменый TodoItem в ChangeLog
     */
    saveChanges(todoItem, stateType) {
        let count = ++this.chagesLog.count;

        this.chagesLog.log[count] = {
            todo_id: todoItem.todoId,
            id: todoItem.id,
            state_id: todoItem.stateId,
            content: todoItem.content,
            hash: todoItem.hash,
            action_type: stateType
        };
    }

    /**
     * @access private
     * @async
     * @return void
     * @desc
     * Инициализирует процесс проверки наличия измнений и отправки их на сервер
     */
    synchronizeChanges() {
        if(this.chagesLog.count > 0 && this.currentRequest.count === 0) {
            this.currentRequest = Object.assign({}, this.chagesLog);
            this.chagesLog = Object.assign({}, { count: 0, log: {}});

            this.requestData = JSON.stringify(this.currentRequest);
            this.sendRequest();
        }

        this.initDelaySynchronization(this.timeout);
    }

    /**
     * @access private
     * @return void
     * @desc
     * Метод для генерации запросов на сервер
     */
    sendRequest() {
        let xhr = new XMLHttpRequest(),
            requestPath = this.endpoint;

        xhr.onload = (xhr) => {
            this.todoItemsIdUpdateCallback(xhr);
        };
        xhr.onabort = () => {
            setTimeout(() => {
                this.sendRequest().bind(this);
            }, timeout)
        };
        xhr.onerror = () => {
            setTimeout(() => {
                this.sendRequest().bind(this);
            }, timeout)
        };
        xhr.open("POST", requestPath, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.send(this.requestData);
    }

    /**
     * @access public
     * @return void
     * @desc
     * Сбрасывает состояние текущего запроса тем самым подготавливая репозиторий
     * к отправке новых запросов
     */
    resetCurrentRequest() {
        this.currentRequest = Object.assign({}, { count: 0, log: {}});
    }
}