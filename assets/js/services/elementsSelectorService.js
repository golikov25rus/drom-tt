/**
 * Creator: Denis Golikov
 * Date: 07.03.2019
 * Time: 14:19
 */

/**
 * @desc
 * Вспомогательный сервис для быстрого селекта элементов.
 * Основной метод класса {@see ElementSelector.get}
 */
export default class ElementSelector {
    constructor() {
        /**
         * @access private
         * @type {{FIRST: {function}, ALL: {function}}}
         * @desc
         * Указатели на методы способа выборки
         */
        this.selectMode = {
            FIRST: this.getElement,
            ALL: this.getElements
        }
    }

    /**
     * @access public
     * @param {array|object|string} elements - селекторы
     * @param {string} [mode = FIRST] - способ выборки (FIRST|ALL)
     * @return {Element|NodeList|{NodeList}|null}
     * @desc
     *  Основной метод класса для получения элемента или коллекций элементов из строкового селектора.
     *  Работает рекурсивно с погружением до тех пор, пока не будет найдена строка
     */
    get(elements, mode = 'FIRST') {
        let result = {};
        switch(typeof elements) {
            case 'string':
                result = this.selectMode[mode](elements);
                break;
            case 'object':
                if(Array.isArray(elements)) {
                    result = [];
                    for(let value of elements) {
                        result.push(this.get(value, mode));
                    }
                } else {
                    for(let [key, value] of Object.entries(elements)) {
                        result[key] = this.get(value, mode)
                    }
                }
                break;
        }
        return result;
    }

    /**
     * @access private
     * @borrows document
     * @param {string} selector - строковый селектор
     * @return {Element|null}
     * @desc
     *  Метод выборки первого попавшегося элемента (mode = FIRST)
     */
    getElement(selector) {
        return document.querySelector(selector);
    }

    /**
     * @access private
     * @borrows document
     * @param {string} selector - строковый селектор
     * @return {Element|NodeList|null}
     * @desc
     *  Метод выборки всех элементов (mode = ALL)
     */
    getElements(selector) {
        return document.querySelectorAll(selector);
    }

}