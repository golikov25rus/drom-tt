/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App
 * @class TodoRender
 * @desc
 * Класс рендеринга
 */
export default class TodoRender {

    /**
     * @access public
     * @param {ElementSelector} selector - сервис селектов
     * @param {Function} template - View для TodoItem-a
     * @param {string} wrapper - селектор враппера TodoItem-ов
     */
    constructor({selector, template, wrapper}) {
        /**
         * @access private
         * @type {ElementSelector}
         */
        this.selector = selector;

        /**
         * @access private
         * @type {Function}
         */
        this.template = template;

        /**
         * @access private
         * @type {Element}
         */
        this.wrapper = this.selector.get(wrapper);

        /**
         * @access public
         * @type {int}
         * @desc
         * id статуса TodoItem-ов, который необходимо рендрерить при обновлении
         */
        this.showStateId = 0;
    }

    /**
     * @access public
     * @return void
     * @desc
     * Очищает враппер TodoItem-ов
     */
    cleanWrapper() {
        while(this.wrapper.firstChild) {
            this.wrapper.firstChild.remove()
        }
    }

    /**
     * @access public
     * @param {Object} todoItems - коллекция TodoItem-ов
     * @return {Object} - отрендеренная коллекция
     * @desc
     * Создает коллецию View
     */
    createElements(todoItems) {
        let result = {};
        for(let todoItem of Object.values(todoItems)) {
            result[todoItem.hash] = this.createElement(todoItem);
        }
        return result;
    }

    /**
     * @access public
     * @param {Object} todoItem
     * @return {Element}
     * @desc
     * Рендерит TodoItem
     */
    createElement(todoItem) {
        let itemElement = document.createElement('li');
        this.wrapper.appendChild(itemElement);
        itemElement.outerHTML = this.template(todoItem);
        return this.wrapper.lastElementChild;
    }

    /**
     * @access public
     * @param {array} elements - массив елементов
     * @param {null|boolean} [state = null] - признак добавить или убрать класс
     * @param {string}[className = hide] - название класса
     * @return void
     * @desc
     * Переключает / присваивает / удаляет классы у html элементов
     * Если state не указан, то метод попробует переключить указанный класс
     */
    toggleElementsClass([...elements], state = null, className = 'hide') {
        for(let element of Object.values(elements)) {
            switch (state) {
                case null:
                    if(element.classList.contains(className)) {
                        element.classList.remove(className);
                    } else {
                        element.classList.add(className);
                    }
                    break;
                case true:
                    element.classList.add(className);
                    break;
                case false:
                    element.classList.remove(className);
                    break;
            }
        }
    }

    /**
     * @access public
     * @param {Object} todoItems - коллекция TodoItem-ов
     * @return void
     * @desc
     * Фильтрует отображение TodoItem-ов по статусу this.showStateId
     */
    refreshView(todoItems) {
        let toShowElements = [],
            toHideElements = [],
            stateId = this.showStateId;

        for(let todoItem of Object.values(todoItems)) {
            let element = todoItem.wrapperElement;
            if(todoItem.stateId === stateId || stateId === 0) {
                toShowElements.push(element)
            } else {
                toHideElements.push(element)
            }
        }
        this.toggleElementsClass(toShowElements, false);
        this.toggleElementsClass(toHideElements, true);
    }
}