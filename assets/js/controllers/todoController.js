/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App
 * @class TodoController
 * @desc
 * Основной класс приложения для страницы /todo/
 */
export default class TodoController {
    /**
     * @access public
     * @param {TodoRepository} repository
     * @param {{selector: ElementSelector, render: ElementSelector}} services
     * @param {Object} selectors - объект строковых селекторов
     */
    constructor({repository, services, selectors}) {
        /**
         * @access private
         * @type {TodoRepository}
         */
        this.repository = repository;
        /**
         * @access private
         * @type {{selector: ElementSelector, render: ElementSelector}}
         */
        this.services = services;
        /**
         * @access private
         * @type {Object}
         */
        this.elements = this.services.selector.get(selectors);
        /**
         * @access private
         * @type {{TodoItem}}
         * @desc
         * Коллекция TodoItem-ов
         */
        this.todoItems = this.repository.getItemsFromWrapper(this.services.render.wrapper);
        /**
         * @access private
         * @type {int}
         */
        this.todoId = parseInt(this.services.render.wrapper.dataset.todo);

        this.run();
    }

    /**
     * @access private
     * @return void
     * @desc
     * Инициализация логики
     */
    run() {
        this.services.render.cleanWrapper();
        let uiElements = this.services.render.createElements(this.todoItems);

        for(let [hash, element] of Object.entries(uiElements)) {
            this.bindElementToItem(element, hash);
            this.setListenersToItem(this.todoItems[hash]);
        }
        
        this.repository.todoItemsIdUpdateCallback = this.updateTodoItemsId.bind(this);
        this.updateUiChanges();

        let elements = this.elements;
        elements.input.addEventListener('keydown', event => this.eventHandle(event, 'keyDown'));
        elements.buttons.toggle.addEventListener('click', event => this.eventHandle(event, 'toggleItemsState'));
        elements.buttons.clear.addEventListener('click', event => this.eventHandle(event, 'deleteCompleted'));
        elements.filters.all.addEventListener('click', event => this.eventHandle(event, 'renderAll'));
        elements.filters.active.addEventListener('click', event => this.eventHandle(event, 'renderActive'));
        elements.filters.completed.addEventListener('click', event => this.eventHandle(event, 'renderCompleted'));
    }

    /**
     * @access private
     * @return void
     * @desc
     * Связывает HTMLElement с моделью
     */
    bindElementToItem(element, hash) {
        this.todoItems[hash].element = element;
    }

    /**
     * @access private
     * @return TodoItem
     * @desc
     * Возвращает модель по элементу
     */
    getTodoItemByElement(element) {
        return this.todoItems[element.dataset.item];
    }

    /**
     * @access private
     * @return void
     * @desc
     * Навешивает слушателей на внутренние элементы модели
     */
    setListenersToItem(todoItem) {
        todoItem.wrapperElement.addEventListener('dblclick', event => this.eventHandle(event, 'doubleClickItem'));
        todoItem.inputElement.addEventListener('blur', event => this.eventHandle(event, 'blurItem'));
        todoItem.checkboxElement.addEventListener('click', event => this.eventHandle(event, 'toggleItemState'));
        todoItem.deleteButtonElement.addEventListener('click', event => this.eventHandle(event, 'deleteItem'));
    }

    /**
     * @access private
     * @borrows App~TodoRepository
     * @borrows App~TodoRender
     * @return void
     * @desc
     * Роутинг событий
     */
    eventHandle(event, eventType) {
        switch(eventType) {
            case 'keyDown': if(event.target.value.length > 0 && event.keyCode === 13) {
                let todoItem = this.repository.createNewTodoItem(event.target.value, this.todoId),
                    uiElement = this.services.render.createElement(todoItem);
                this.todoItems[todoItem.hash] = todoItem;
                this.bindElementToItem(uiElement, todoItem.hash);
                this.setListenersToItem(this.todoItems[todoItem.hash]);
                event.target.value = '';
                this.handleStateUpdate(todoItem, 'create');
            }
                break;
            case 'toggleItemState': {
                let todoItem = this.getTodoItemByElement(event.target);
                todoItem.toggleState();
                this.handleStateUpdate(todoItem, 'update');
            }
                break;
            case 'toggleItemsState': {
                for(let todoItem of Object.values(this.todoItems)) {
                    todoItem.state = event.target.checked ? 2 : 1;
                    this.handleStateUpdate(todoItem, 'update');
                }
            }
                break;
            case 'deleteItem': {
                let todoItem = this.getTodoItemByElement(event.target);
                todoItem.selfElementDelete();
                this.handleStateUpdate(todoItem, 'delete');
                delete this.todoItems[todoItem.hash];
            }
                break;
            case 'blurItem': {
                let todoItem = this.getTodoItemByElement(event.target);
                todoItem.content = todoItem.input;
                this.handleStateUpdate(todoItem, 'update');
            }
                break;
            case 'doubleClickItem': {
                let todoItem = this.getTodoItemByElement(event.currentTarget);
                todoItem.input = todoItem.content;
            }
                break;
            case 'deleteCompleted': {
                for(let todoItem of Object.values(this.todoItems)) {
                    if(todoItem.stateId === 2) {
                        todoItem.selfElementDelete();
                        this.handleStateUpdate(todoItem, 'delete');
                        delete this.todoItems[todoItem.hash];
                    }
                }
            }
                break;
            case 'renderAll': {
                let render = this.services.render;
                render.showStateId = 0;
                render.toggleElementsClass(Object.values(this.elements.filters), false, 'selected');
                render.toggleElementsClass([event.target], true, 'selected');
            }
                break;
            case 'renderActive': {
                let render = this.services.render;
                render.showStateId = 1;
                render.toggleElementsClass(Object.values(this.elements.filters), false, 'selected');
                render.toggleElementsClass([event.target], true, 'selected');
            }
                break;
            case 'renderCompleted': {
                let render = this.services.render;
                render.showStateId = 2;
                render.toggleElementsClass(Object.values(this.elements.filters), false, 'selected');
                render.toggleElementsClass([event.target], true, 'selected');
            }
                break;
        }

        let needToSyncUiChanges = new Set(['toggleItemState', 'toggleItemsState',
                                          'deleteItem', 'deleteItems', 'blurItem', 'deleteCompleted']);

        if(eventType === 'keyDown' && event.keyCode === 13 || needToSyncUiChanges.has(eventType)) {
            this.updateUiChanges();
        }
        this.services.render.refreshView(this.todoItems);
    }

    /**
     * @access private
     * @borrows App~TodoRender
     * @return void
     * @desc
     * Обновляет UI при измении состояния коллекции TodoItem-ов
     */
    updateUiChanges() {
        let render = this.services.render,
            totalCount = Object.keys(this.todoItems).length,
            activeCount = 0;
        for(let todoItem of Object.values(this.todoItems)) {
            activeCount += todoItem.stateId === 1 ? 1 : 0;
        }
        this.elements.count.textContent = activeCount;
        this.elements.buttons.toggle.checked = activeCount === 0;
        if(!totalCount) {
            render.toggleElementsClass(Object.values(this.elements.buttons), true);
            render.toggleElementsClass(Object.values(this.elements.filters), true);
            render.toggleElementsClass([this.elements.count.parentElement, this.elements.footer], true);
        } else {
            render.toggleElementsClass(Object.values(this.elements.buttons), false);
            render.toggleElementsClass(Object.values(this.elements.filters), false);
            render.toggleElementsClass([this.elements.count.parentElement, this.elements.footer], false);
        }
    }

    /**
     * @access private
     * @callback
     * @borrows App~TodoRepository
     * @param {TodoItem} todoItem
     * @param {int} stateType
     * @return void
     * @desc
     * Обработчик измениня состояния TodoItem
     */
    handleStateUpdate(todoItem, stateType) {
        this.repository.saveChanges(todoItem, stateType);
    }

    /**
     * @access private
     * @async
     * @callback
     * @borrows App~TodoRepository
     * @param {XMLHttpRequestResponseType} data
     * @return void
     * @desc
     * Обработчик ответов от сервера
     * Осуществляет редирект на страницу авторизации, если серверу не удалось авторизовать пользователя
     * TODO: вынести редирект в отдельный метод, этот метод назвать handleResponse
     */
    updateTodoItemsId(data) {
        let response = JSON.parse(data.target.response);

        if(response.state === 'redirect') {
            setTimeout(() => {
                window.location.replace(response.data.path)
            },response.data.timeOut)
        }

        for(let createdItem of Object.values(response.data)) {
            if(createdItem.hash in this.todoItems) {
                this.todoItems[createdItem.hash].id = createdItem.id;
            }
            for(let [id, changedItem] of Object.entries(this.repository.chagesLog.log)) {
                if(changedItem.hash === createdItem.hash) {
                    this.repository.chagesLog.log[id].id = createdItem.id;
                }
            }
        }
        this.repository.resetCurrentRequest();
    }
}