/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

import ElementSelector from '../services/elementsSelectorService';

/**
 * @namespace App
 * @class AuthController
 * @desc
 * Класс контроллера страницы входа/регистрации
 */
export default class AuthController {
    /**
     * @access public
     * @param {{login: string, password: string}} inputs - селекторы инпутов логина и пароля
     * @param {{signUp: string, login: string}} buttons - селекторы кнопок входа и регистрации
     * @param {string} form - селектор враппера
     * @param {string} messageBox - селектор блока сообщений
     * @param {string} restEndpoint - endpoint restApi
     */
    constructor({inputs, buttons, form, messageBox,restEndpoint:endpoint}) {
        /**
         * @access private
         * @type {ElementSelector}
         */
        this.selector = new ElementSelector();

        /**
         * @access private
         * @type {Element}
         */
        this.form = this.selector.get(form);

        /**
         * @access private
         * @type {NodeList}
         */
        this.inputs = this.selector.get(inputs);

        /**
         * @access private
         * @type {NodeList}
         */
        this.buttons = this.selector.get(buttons);

        /**
         * @access private
         * @type {Element}
         */
        this.messageBox = this.selector.get(messageBox);

        /**
         * @access private
         * @type {string}
         */
        this.endpoint = endpoint;

        this.run()
    }

    /**
     * @access private
     * @return void
     * @desc
     * Инициализация логики
     */
    run() {
        this.form.addEventListener('click', event => this.clickHandler(event));
        this.form.addEventListener('input', event => this.inputHandler(event));
    }

    /**
     * @access private
     * @param {string} string
     * @return {string}
     * @desc
     * Обрезает пробельные и цифровые символы в начале и конце строки
     */
    inputTrim(string) {
        return string.replace(/^[\s]+|[\s]+$/g, '')
    }

    /**
     * @access private
     * @param {Event} event
     * @return void
     * @desc
     * Обрабатывает пользовательский ввод
     */
    inputHandler(event) {
        if (Object.values(this.inputs).indexOf(event.target) >= 0) {
            this.messageBox.textContent = '';
            let input = event.target;
            input.value = this.inputTrim(input.value)
        }
    }

    /**
     * @access private
     * @param {Event} event
     * @return void
     * @desc
     * Обрабатывает пользовательские клики
     */
    clickHandler(event) {
        if (Object.values(this.buttons).indexOf(event.target) >= 0) {
            if(this.isValidInput()) {
                this.messageBox.textContent = '';
                this.sendRequest(this.inputs.login.value, this.inputs.password.value, event.target.dataset.route)
            } else {
                this.messageBox.textContent = 'Поля логина и пароля должны быть заполнены'
            }
        }
    }

    /**
     * @access private
     * @return {boolean}
     * @desc
     * Проверяет что логин и пароль не пустые
     */
    isValidInput() {
        return this.inputs.login.value.length > 0
            && this.inputs.password.value.length > 0
    }

    /**
     * @access private
     * @async
     * @param {string} login
     * @param {string} password
     * @param {string} route
     * @return void
     * @desc
     * Метод для генерации запросов на сервер
     */
    sendRequest(login, password, route) {
        let xhr = new XMLHttpRequest(),
            requestPath = this.endpoint + route,
            data = JSON.stringify({
                    login: encodeURIComponent(login),
                    password: encodeURIComponent(password)
            });
        xhr.onload = (xhr) => {
            if(xhr.target.status === 500) {
                this.showErrorMessage();
                return;
            }
            let response = JSON.parse(xhr.target.response);
            this.messageBox.textContent = response.message;
            if(response.state === 'redirect') {
                setTimeout(() => {
                    window.location.replace(response.data.path)
                },response.data.timeOut)
            }
        };
        xhr.onabort = () => {
            this.showErrorMessage()
        };
        xhr.onerror = () => {
            this.showErrorMessage()
        };
        xhr.open("POST", requestPath, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.send(data);
    }

    /**
     * @access private
     * @return void
     * @desc
     * Отображает сообщение об ошибке
     */
    showErrorMessage() {
        this.messageBox.textContent = 'Ошибка соединения с сервером';
    }
}