export var TodoView = (todoItem) => {
    return`
    <li class="${todoItem.stateName}" data-item="${todoItem.hash}">
        <div class="view">
            <input class="toggle" data-item="${todoItem.hash}" type="checkbox" ${todoItem.stateId === 2 ? 'checked' : ''}>
            <label data-item="${todoItem.hash}">${todoItem.content}</label>
            <button class="destroy" data-item="${todoItem.hash}"></button>
        </div>
        <input class="edit" data-item="${todoItem.hash}">
    </li>
    `
}