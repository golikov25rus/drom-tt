<?php
/**
 * Creator:  Denis Golikov
 * Date: 02.03.2019
 * Time: 23:52
 */

//Define root path
const ROOT = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
//Load configuration
require_once ROOT.'config'.DIRECTORY_SEPARATOR.'app.php';
//Load composer autoload
require_once ROOT.'src'.DIRECTORY_SEPARATOR.'Vendors'.DIRECTORY_SEPARATOR.'autoload.php';