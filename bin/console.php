<?php
/**
 * Creator:  Denis Golikov
 * Date: 12.03.2019
 * Time: 19:57
 */

use App\Common\DAO;

//Обработка аргументов консоли
$command = $argv[1]?? null;
if($command !== 'migrate') {
    echo "Available commands: \n migrate - start common migration process";
    exit();
}

//Автозагрузчик
$bootstrap = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap'.DIRECTORY_SEPARATOR;
require_once $bootstrap.'autoload.php';

$migrationsPath = ROOT.'bin'.DIRECTORY_SEPARATOR.'commonMigration.sql';

$db = new DAO();
$result = $db->query(file_get_contents($migrationsPath));
echo $result;
exit();