DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS users_keys;
DROP TABLE IF EXISTS todo;
DROP TABLE IF EXISTS todo_items;
DROP TABLE IF EXISTS todo_items_states;

CREATE TABLE users
(id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(300) NOT NULL,
updated_at TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
created_at TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
PRIMARY KEY (id));

CREATE TABLE users_keys
(id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
login VARCHAR(300) NOT NULL,
password VARCHAR(300) NOT NULL,
token VARCHAR(300) DEFAULT '',
hash VARCHAR(300) NOT NULL,
salt VARCHAR(300) NOT NULL,
updated_at TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
created_at TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
PRIMARY KEY (id));

CREATE TABLE todo
(id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
updated_at TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
created_at TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
PRIMARY KEY (id));

CREATE TABLE todo_items
(id INT NOT NULL AUTO_INCREMENT,
todo_id INT NOT NULL,
state_id INT NOT NULL,
content VARCHAR(1000) NOT NULL,
updated_at TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
created_at TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
PRIMARY KEY (id));

CREATE TABLE todo_items_states
(id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(300) NOT NULL,
alias VARCHAR(300) NOT NULL,
updated_at TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
created_at TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6),
PRIMARY KEY (id));

INSERT INTO todo_items_states (name, alias)
VALUES ('active', 'Активный'), ('completed', 'Завершенный');