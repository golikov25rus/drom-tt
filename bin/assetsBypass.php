<?php
/**
 * Creator:  Denis Golikov
 * Date: 13.03.2019
 * Time: 1:49
 */


$pattern = '/\.(?:png|jpg|jpeg|gif|js|css|map|json|html)$/';
$request = $_SERVER['REQUEST_URI'];

if (preg_match($pattern, $request)) {

    if (!file_exists(ROOT.$request)) {
        return false;
    }

    if(preg_match('/\.(js)$/', ROOT.$request)) {
        header('Content-type: application/javascript');
    }

    if(preg_match('/\.(css)$/', ROOT.$request)) {
        header('Content-type: text/css');
    }
    readfile(ROOT.$request);
    exit();
}