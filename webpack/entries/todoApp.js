// JS AND CSS ASSETS IMPORT
import TodoController from '../../assets/js/controllers/todoController.js';
import TodoItem from '../../assets/js/models/todoItemModel.js';
import TodoRepository from '../../assets/js/repositories/todoRepository.js';
import ElementSelector from '../../assets/js/services/elementsSelectorService';
import TodoRender from '../../assets/js/services/todoRenderService.js';
import {TodoView} from '../../assets/js/views/todoView.js';

// INIT CLIENT APPLICATION
((w) => {
    w.onload = () => {
        new TodoController({
            repository: new TodoRepository({
                model: TodoItem,
                endpoint: '/rest/todo',
                pollingTimeout: 5000
            }),
            services: {
                selector: new ElementSelector(),
                render: new TodoRender({
                    selector: new ElementSelector(),
                    template: TodoView,
                    wrapper: '.todo-list'
                })
            },
            selectors: {
                input: '.new-todo',
                buttons: {
                    clear: '.clear-completed',
                    toggle: '.toggle-all',
                    toggleLabel: '[for=toggle-all]'
                },
                filters: {
                    all: '.filter-all',
                    active: '.filter-active',
                    completed: '.filter-completed'
                },
                count: '.count',
                footer: '.footer'
            }
        })
    }
})(window);
