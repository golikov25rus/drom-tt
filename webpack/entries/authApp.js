// JS AND CSS ASSETS IMPORT
import '../../assets/css/auth.css';
import AuthController from '../../assets/js/controllers/authController.js';

// INIT CLIENT APPLICATION
((w) => {
    w.onload = () => {
        new AuthController({
            inputs: {
                login: '#login',
                password: '#password'
            },
            buttons: {
                signUp: '#signUpButton',
                login: '#loginButton'
            },
            form: '#form',
            messageBox: '#messageBox',
            restEndpoint: '/rest/'
        })
    }
})(window);