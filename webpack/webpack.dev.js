const merge = require('webpack-merge'),
      common = require('./webpack.common.js'),
      path = require('path');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'inline-source-map',
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    },
    devServer: {
        host: 'localhost',
        port: 9050,
        publicPath: '/public',
        proxy: {
            '/': {
                context: [
                    '/**',
                    '!**/public/js/**',
                    '!**/public/css/**'
                ],
                target: 'http://localhost:8080',
                secure: false,
                overlay: {
                    warnings: true,
                    errors: true
                }
            }
        }
    }
});
