const merge = require('webpack-merge'),
      common = require('./webpack.common.js'),
      UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
      OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin"),
	  CssNano = require('cssnano');


	module.exports = merge(common, {
		  mode: 'production',
		  optimization: {
				minimizer: [
					new UglifyJsPlugin({
						cache: true,
						parallel: true,
						sourceMap: true
					}),
					new OptimizeCSSAssetsPlugin({
						assetNameRegExp: /\.css$/g,
						cssProcessor: CssNano
					})
				]
		  },
		module: {
			rules: [
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
				}
			]
		}
	});
