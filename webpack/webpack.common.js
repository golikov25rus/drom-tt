const path = require('path'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
	  MiniCssExtractPlugin = require("mini-css-extract-plugin"),
 	  CleanWebpackPlugin = require('clean-webpack-plugin'),
 	  HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

module.exports = {
	entry: {
		common: ['./webpack/entries/common.js'],
        authApp: ['./webpack/entries/authApp.js'],
        todoApp: ['./webpack/entries/todoApp.js']
    },
	plugins: [
		new CleanWebpackPlugin(['js, css'], { root: path.resolve(__dirname, '../public')}),

		new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            inject: false,
			template: path.resolve(__dirname, '../assets/html/templates/index.html.twig'),
			filename: path.resolve(__dirname, '../assets/html/injected/index.html.twig'),
            chunks: ['common', 'authApp']
    	}),
        new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            inject: false,
            template: path.resolve(__dirname, '../assets/html/templates/todo.html.twig'),
            filename: path.resolve(__dirname, '../assets/html/injected/todo.html.twig'),
            chunks: ['common', 'todoApp']
        }),

		new MiniCssExtractPlugin({
			filename: 'css/[hash]-[name]-drom.css',
			path: path.resolve(__dirname, '../public')
		}),

        new HtmlWebpackHarddiskPlugin(),
   ],
	output: {
		filename: 'js/[hash]-[name]-drom.js',
		path: path.resolve(__dirname, '../public'),
		publicPath: '/public'
	},
	module: {
		rules: [
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [{
					loader: 'file-loader',
                    options: {
                        name: 'images/[hash]-[name]-drom.[ext]'
                    }
                }],
			},
			{
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
	         	use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[hash]-[name]-drom.[ext]'
                    }
                }]
			},
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
		]
	}
};