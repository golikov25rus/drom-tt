<?php
/**
 * Creator:  Denis Golikov
 * Date: 03.03.2019
 * Time: 0:35
 */

//Режим разработки
const DEV_MODE = true;

//Конфигурация путей
const ROUTES = ROOT.'config'.DIRECTORY_SEPARATOR.'routes.yaml';
const TEMPLATES_PATH = ROOT.'assets'.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'injected'.DIRECTORY_SEPARATOR;
const ASSETS_PATH = ROOT.'public';
const CACHE_PATH = ROOT.'var';

//Конфигурация Twig
const TWIG_CONFIG = [
    'cache' => CACHE_PATH.DIRECTORY_SEPARATOR.'twig',
    'debug' => DEV_MODE,
    'auto_reload' => DEV_MODE
];

//Настройки подключения к бд
const DB = [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'port'      => 3306,
    'database'  => 'drom',
    'username'  => 'drom',
    'password'  => '11135',
    'charset'   => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
];
