<?php
/**
 * Creator:  Denis Golikov
 * Date: 09.03.2019
 * Time: 18:55
 */

namespace App\Entities;


/**
 * Class Todo
 * @package App\Entities
 */
class Todo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;
    /**
     * @var array
     */
    private $todoItems = [];

    /**
     * @param int $id
     */
    public function __construct(int $id, int $userId)
    {
        $this->id = $id;
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return array
     */
    public function getTodoItems(): array
    {
        return $this->todoItems;
    }

    /**
     * @param array $todoItems
     */
    public function setTodoItems(array $todoItems): void
    {
        $this->todoItems = $todoItems;
    }

    /**
     * @return int
     */
    public function getCompletedCount(): int
    {
        $result = 0;
        foreach ($this->todoItems as $item) {
            $result += $item->getStateId() === 2 ? 1 : 0;
        }
        return $result;
    }
}