<?php
/**
 * Creator:  Denis Golikov
 * Date: 07.03.2019
 * Time: 1:17
 */

namespace App\Entities;
use App\Entities\UserKeys;
use App\Entities\Todo;


/**
 * Class User
 * @package App\Entities
 */
class User
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var UserKeys
     */
    private $keys;
    /**
     * @var Todo
     */
    private $todo;

    /**
     * User constructor.
     * @param int $id
     * @param string $name
     * @param UserKeys $keys
     */
    public function __construct(int $id, string $name, UserKeys $keys)
    {
        $this->id = $id;
        $this->name = $name;
        $this->keys = $keys;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return UserKeys
     */
    public function getKeys(): UserKeys
    {
        return $this->keys;
    }

    /**
     * @param UserKeys $keys
     */
    public function setKeys(UserKeys $keys): void
    {
        $this->keys = $keys;
    }

    /**
     * @return Todo
     */
    public function getTodo(): Todo
    {
        return $this->todo;
    }

    /**
     * @param Todo $todo
     */
    public function setTodo(Todo $todo): void
    {
        $this->todo = $todo;
    }
}