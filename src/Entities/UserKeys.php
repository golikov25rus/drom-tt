<?php
/**
 * Creator:  Denis Golikov
 * Date: 08.03.2019
 * Time: 5:20
 */

namespace App\Entities;


/**
 * Class UserKeys
 * @package App\Entities
 */
class UserKeys
{
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $hash;
    /**
     * @var string
     */
    private $salt;

    /**
     * UserKeys constructor.
     * @param string $login
     * @param string $password
     * @param string $salt
     * @param string $hash
     * @param string $token
     */
    public function __construct(string $login, string $password, string $salt, string $hash, string $token)
    {
        $this->login = $login;
        $this->password = $password;
        $this->token = $token;
        $this->hash = $hash;
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }
}