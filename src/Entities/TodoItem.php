<?php
/**
 * Creator:  Denis Golikov
 * Date: 09.03.2019
 * Time: 18:56
 */

namespace App\Entities;


/**
 * Class TodoItem
 * @package App\Entities
 */
class TodoItem
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $todoId;
    /**
     * @var int
     */
    private $stateId;
    /**
     * @var string
     */
    private $stateName;
    /**
     * @var string
     */
    private $content;

    /**
     * TodoItem constructor.
     * @param int $id
     * @param int $todoId
     * @param int $stateId
     * @param string $stateName
     * @param string $content
     */
    public function __construct(int $id, int $todoId, int $stateId, string $stateName, string $content)
    {
        $this->id = $id;
        $this->todoId = $todoId;
        $this->stateId = $stateId;
        $this->stateName = $stateName;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTodoId(): int
    {
        return $this->todoId;
    }


    /**
     * @return string
     */
    public function getStateName(): string
    {
        return $this->stateName;
    }

    /**
     * @return int
     */
    public function getStateId(): int
    {
        return $this->stateId;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}