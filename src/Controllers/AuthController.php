<?php
/**
 * Creator:  Denis Golikov
 * Date: 08.03.2019
 * Time: 8:01
 */

namespace App\Controllers;

use App\Services\AuthService;
use App\Repositories\UserRepository;
use App\Entities\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class AuthController
 * @package App\Controllers
 */
class AuthController extends AController
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var AuthService
     */
    private $authService;
    /**
     * @var string
     */
    private $template = 'index.html.twig';
    /**
     * @var array
     */
    private $data = [];

    /**
     * AuthController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->repository = new UserRepository();
        $this->authService = new AuthService();
    }

    /**
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function preparePage(): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/html');

        if (!file_exists(TEMPLATES_PATH.$this->template)) {
            $response->setStatusCode(404);
            $response->setContent('Resource not found');
            return $response;
        }

        $response->setContent($this->render($this->data, $this->template));
        return $response;
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function sign(): JsonResponse
    {
        $data = json_decode($this->request->getContent(), true);
        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->isValidInput($data)) {
            $response->setJson(json_encode(['state' => 'invalidData',
                                            'message' => 'Поля логина и пароля должны быть заполнены']));
            return $response;
        }

        if ($this->repository->isLoginExist($data['login'])) {
            $response->setJson(json_encode(['state' => 'undefined',
                                            'message' => 'Пользователь с таким именем уже существует']));
            return $response;
        }

        $login = $data['login'];
        $password = $data['password'];

        $keys = $this->authService->createKeys($login, $password);
        $user = $this->repository->createUser($keys);

        $this->setCookie($user, $response);
        $response->setJson(json_encode(['state' => 'redirect',
                                        'message' => 'Регистрация...',
                                        'data' => ['path' => '/todo/', 'timeOut' => 3000]]));

        return $response;
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function login(): JsonResponse
    {
        $data = json_decode($this->request->getContent(), true);
        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->isValidInput($data)) {
            $response->setContent(json_encode(['state' => 'invalidData',
                                               'message' => 'Поля логина и пароля должны быть заполнены']));
            return $response;
        }

        if (!$this->repository->isLoginExist($data['login'])) {
            $response->setContent(json_encode(['state' => 'undefined',
                                               'message' => 'Не верные имя пользователя или пароль']));
            return $response;
        }

        $login = $data['login'];
        $password = $data['password'];
        $user = $this->repository->getUserByLogin($login);

        if (!$this->authService->isValidPasswords($user, $password)) {
            $response->setContent(json_encode(['state' => 'undefined',
                                               'message' => 'Не верные имя пользователя или пароль']));
            return $response;
        }

        $keys = $this->authService->createKeys($login, $password);

        $user->setKeys($keys);
        $this->repository->updateUserKeys($user);
        $this->setCookie($user, $response);
        $response->setContent(json_encode(['state' => 'redirect',
                               'message' => 'Авторизация...',
                               'data' => ['path' => '/todo/', 'timeOut' => 3000]]));

        return $response;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isValidInput(array $data): bool
    {
        return
            isset($data['login'])
            && strlen($data['login']) > 0
            && isset($data['password'])
            && strlen($data['password']) > 0
            && preg_match('/\s/', $data['login']) == 0
            && preg_match('/\s/', $data['password']) == 0;
    }

    /**
     * @param User $user
     * @param Response $response
     */
    private function setCookie(User $user, Response $response): void
    {
        $cookie = new Cookie('todo-token', $user->getKeys()->getToken());
        $response->headers->setCookie($cookie);
    }
}