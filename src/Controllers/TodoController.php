<?php
/**
 * Creator:  Denis Golikov
 * Date: 08.03.2019
 * Time: 15:15
 */

namespace App\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class TodoController
 * @package App\Controllers
 */
class TodoController extends AController
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var TodoRepository
     */
    private $todoRepository;
    /**
     * @var string
     */
    private $template = 'todo.html.twig';
    /**
     * @var array
     */
    private $data = [];

    /**
     * TodoController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->userRepository = new UserRepository();
        $this->todoRepository = new TodoRepository();
    }

    /**
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function preparePage(): Response
    {
        $response = new Response();
        $db = $this->todoRepository;
        $response->headers->set('Content-Type', 'text/html');
        $token = $this->request->cookies->get('todo-token');

        if (!file_exists(TEMPLATES_PATH.$this->template)) {
            $response->setStatusCode(404);
            return $response;
        }

        if (!$token) {
            $response = new RedirectResponse('/');
            return $response;
        }

        $user = $this->userRepository->getUserByToken($token);

        if (!$user) {
            $response = new RedirectResponse('/');
            return $response;
        }

        if (!$db->isUserHaveTodo($user)) {
            $user->setTodo($db->createTodo($user));
        } else {
            $user->setTodo($db->getTodoByUser($user));
        }

        $this->data['user'] = $user;

        $response->setContent($this->render($this->data, $this->template));
        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function handleAjax(): JsonResponse
    {
        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');
        $token = $this->request->cookies->get('todo-token');

        if (!$token) {
            $response->setJson(json_encode(['state' => 'redirect',
                                            'message' => 'Выход...',
                                            'data' => ['path' => '/', 'timeOut' => 3000]]));
            return $response;
        }

        $changeLog = json_decode($this->request->getContent(), true)['log'];
        $createdTodoItems = $this->handleChangelog($changeLog);

        $response->setJson(json_encode(['state' => 'created',
                                        'data' => $createdTodoItems]));
        return $response;
    }

    /**
     * @param array $data
     * @return array
     */
    private function handleChangelog(array $data): array
    {
        $db = $this->todoRepository;

        $toCreate = array_filter($data, function($value, $key) { return $value['action_type'] === 'create';}, ARRAY_FILTER_USE_BOTH);
        $createdTodoItems = $db->createItems($toCreate);

        foreach ($data as $id => $item) {
            foreach ($createdTodoItems as $createdItem) {
                if ($item['hash'] === $createdItem['hash']) {
                    $data[$id]['id'] = $createdItem['id'];
                }
            }
        }

        $toUpdate = array_filter($data, function($value, $key) { return $value['action_type'] === 'update';}, ARRAY_FILTER_USE_BOTH);
        $toDelete = array_filter($data, function($value, $key) { return $value['action_type'] === 'delete';}, ARRAY_FILTER_USE_BOTH);
        $db->updateItems($toUpdate);
        $db->deleteItems($toDelete);
        return $createdTodoItems;
    }
}