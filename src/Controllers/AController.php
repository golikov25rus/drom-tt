<?php
/**
 * Creator:  Denis Golikov
 * Date: 06.03.2019
 * Time: 12:47
 */

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Twig\Loader\FilesystemLoader as TwigLoader;
use Twig\Environment as Twig;

/**
 * Class AController
 * @package App\Controllers
 */
abstract class AController
{
    /**
     * AController constructor.
     * @param Request $request
     */
    abstract public function __construct(Request $request);

    /**
     * @param array $data
     * @param string $template
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(array $data, string $template): string
    {
        $loader = new TwigLoader(TEMPLATES_PATH);
        $render = new Twig($loader, TWIG_CONFIG);
        return $render->render($template, $data);
    }
}