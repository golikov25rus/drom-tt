<?php
/**
 * Creator:  Denis Golikov
 * Date: 03.03.2019
 * Time: 1:51
 */

namespace App\Common;

use \PDO;
use \PDOStatement as Statement;

/**
 * Class DAO
 * @package Common
 */
class DAO
{
    /**
     * Параметры подключения к базе данных
     */
    private const DSN = DB['driver'].':host='.DB['host'].';dbname='.DB['database'];
    private const CHARSET = 'SET NAMES "'.DB['charset'].'"';

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * DAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO(self::DSN, DB['username'], DB['password']);
        $this->pdo->exec(self::CHARSET);
    }

    /**
     * @param string $query
     * @return string
     */
    public function query(string $query): string
    {
       return $this->pdo->query($query)->queryString;
    }

    /**
     * @param string $query
     * @param array $data
     * @return Statement
     */
    public function execute(string $query, array $data): Statement
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($data);
        return $statement;
    }

    /**
     * @return int
     */
    public function getLastInsertId(): int
    {
        return $this->pdo->lastInsertId();
    }
}