<?php
/**
 * Creator:  Denis Golikov
 * Date: 06.03.2019
 * Time: 23:49
 */

namespace App\Repositories;

use App\Entities\User;
use App\Entities\UserKeys;
use App\Common\DAO;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     *Таблица пользователей
     */
    private const USERS_TABLE = 'users';
    /**
     *Таблица паролей пользователей
     */
    private const USERS_KEYS_TABLE = 'users_keys';
    /**
     * @var DAO
     */
    private $database;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->database = new DAO();

    }

    /**
     * @param string $login
     * @return bool
     */
    public function isLoginExist(string $login): bool
    {
        $table = self::USERS_KEYS_TABLE;

        $sql = "SELECT * FROM $table WHERE login = :login LIMIT 1";
        $statement = $this->database->execute($sql, [':login' => $login]);
        return count($statement->fetchAll()) > 0;
    }

    /**
     * @param string $login
     * @return User
     */
    public function getUserByLogin(string $login): User
    {
        $userTable = self::USERS_TABLE;
        $keysTable = self::USERS_KEYS_TABLE;

        $sql = "SELECT * FROM $userTable
                LEFT JOIN $keysTable ON $userTable.id = $keysTable.user_id
                WHERE $keysTable.login = :login";

        $data = $this->database
                     ->execute($sql, [':login' => $login])
                     ->fetch(\PDO::FETCH_ASSOC);

        return $this->createAggregation($data);
    }

    /**
     * @param string $token
     * @return User
     */
    public function getUserByToken(string $token): User
    {
        $userTable = self::USERS_TABLE;
        $keysTable = self::USERS_KEYS_TABLE;

        $sql = "SELECT * FROM $userTable
                LEFT JOIN $keysTable ON $userTable.id = $keysTable.user_id
                WHERE $keysTable.token = :token";

        $data = $this->database
                     ->execute($sql, [':token' => $token])
                     ->fetch(\PDO::FETCH_ASSOC);

        return $this->createAggregation($data);
    }

    /**
     * @param UserKeys $keys
     * @return User
     */
    public function createUser(UserKeys $keys): User
    {
        $userTable = self::USERS_TABLE;
        $keysTable = self::USERS_KEYS_TABLE;

        $sql = "INSERT INTO $userTable (name) VALUE (:login)";
        $this->database->execute($sql, [':login' => $keys->getLogin()]);
        $userId = $this->database->getLastInsertId();

        $sql = "INSERT INTO $keysTable (user_id, login, password, token, hash, salt) 
                VALUE (:user_id, :login, :password, :token, :hash, :salt)";
        $this->database->execute($sql, [
                                  ':user_id' => $userId,
                                  ':login' => $keys->getLogin(),
                                  ':password' => $keys->getPassword(),
                                  ':token' => $keys->getToken(),
                                  ':hash' => $keys->getHash(),
                                  ':salt' => $keys->getSalt()]);
        return new User($userId, $keys->getLogin(), $keys);
    }

    /**
     * @param User $user
     */
    public function updateUserKeys(User $user): void
    {
        $keysTable = self::USERS_KEYS_TABLE;

        $keys = $user->getKeys();

        $sql = "UPDATE $keysTable SET password = :password, token = :token, hash = :hash, salt = :salt
                WHERE user_id = :user_id";
        $this->database->execute($sql, [
            ':user_id' => $user->getId(),
            ':password' => $keys->getPassword(),
            ':token' => $keys->getToken(),
            ':hash' => $keys->getHash(),
            ':salt' => $keys->getSalt()]);
    }

    /**
     * @param array $data
     * @return User
     */
    private function createAggregation(array $data): User
    {
        $keys = new UserKeys($data['login'],
                             $data['password'],
                             $data['salt'],
                             $data['hash'],
                             $data['token']);
        return new User($data['id'], $data['login'], $keys);
    }
}