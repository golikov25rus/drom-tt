<?php
/**
 * Creator:  Denis Golikov
 * Date: 09.03.2019
 * Time: 18:47
 */

namespace App\Repositories;

use App\Entities\User;
use App\Entities\Todo;
use App\Entities\TodoItem;
use App\Common\DAO;


/**
 * Class TodoRepository
 * @package App\Repositories
 */
class TodoRepository
{
    /**
     *Таблица ToDo
     */
    private const TODO_TABLE = 'todo';
    /**
     *Таблица ToDoItem-ов
     */
    private const TODO_ITEM_TABLE = 'todo_items';
    /**
     *Таблица статусов ToDoItem-ов
     */
    private const TODO_ITEM_STATE_TABLE = 'todo_items_states';

    /**
     * @var DAO
     */
    private $database;

    /**
     * TodoRepository constructor.
     */
    public function __construct()
    {
        $this->database = new DAO();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isUserHaveTodo(User $user): bool
    {
        $table = self::TODO_TABLE;
        $userId = $user->getId();

        $sql = "SELECT * FROM $table WHERE user_id = :user_id LIMIT 1";
        $statement = $this->database->execute($sql, [':user_id' => $userId]);
        return count($statement->fetchAll()) > 0;
    }

    /**
     * @param User $user
     * @return Todo
     */
    public function getTodoByUser(User $user): Todo
    {
        $table = self::TODO_TABLE;

        $userId = $user->getId();

        $sql = "SELECT * FROM $table
                WHERE $table.user_id = :user_id";
        $data = $this->database
            ->execute($sql, [':user_id' => $userId])
            ->fetch(\PDO::FETCH_ASSOC);
        $todo = new Todo($data['id'], $userId);
        $todo->setTodoItems($this->getItemsByTodo($todo));
        return $todo;
    }

    /**
     * @param User $user
     * @return Todo
     */
    public function createTodo(User $user): Todo
    {
        $table = self::TODO_TABLE;
        $userId = $user->getId();

        $sql = "INSERT INTO $table (user_id) VALUE (:user_id)";
        $this->database->execute($sql, [':user_id' => $userId]);
        return $this->getTodoByUser($user);
    }

    /**
     * @param array $items
     * @return array
     */
    public function createItems(array $items): array
    {
        $table = self::TODO_ITEM_TABLE;

        foreach($items as $key => $item) {
            $sql = "INSERT INTO $table (todo_id, state_id, content) 
                    VALUE (:todo_id, :state_id, :content)";
            $this->database
                 ->execute($sql, [':todo_id' => $item['todo_id'],
                                 ':content' => $item['content'],
                                 ':state_id' => $item['state_id']]);
            $items[$key]['id'] = $this->database->getLastInsertId();
        }
        return $items;
    }

    /**
     * @param array $items
     */
    public function updateItems(array $items): void
    {
        $table = self::TODO_ITEM_TABLE;

        foreach($items as $item) {
            $sql = "UPDATE $table SET state_id = :state_id, content = :content WHERE id = :id";
            $this->database
                 ->execute($sql, [':id' => $item['id'],
                                  ':content' => $item['content'],
                                  ':state_id' => $item['state_id']]);
        }
    }

    /**
     * @param array $items
     */
    public function deleteItems(array $items): void
    {
        $table = self::TODO_ITEM_TABLE;

        foreach($items as $item) {
            $sql = "DELETE FROM $table WHERE id = :id";
            $this->database
                 ->execute($sql, [':id' => $item['id']]);
        }
    }

    /**
     * @param Todo $todo
     * @return array
     */
    private function getItemsByTodo(Todo $todo): array
    {
        $result = [];
        $todoId = $todo->getId();
        $itemsTable = self::TODO_ITEM_TABLE;
        $statesTable = self::TODO_ITEM_STATE_TABLE;

        $sql = "SELECT items.id, items.todo_id, items.state_id, states.name as state_name, items.content
                FROM $itemsTable as items
                LEFT JOIN $statesTable as states ON states.id = items.state_id 
                WHERE items.todo_id = :todo_id";
        $data = $this->database
                     ->execute($sql, [':todo_id' => $todoId])
                     ->fetchAll(\PDO::FETCH_ASSOC);
        foreach($data as $item) {
            $result[] = new TodoItem($item['id'],
                                     $item['todo_id'],
                                     $item['state_id'],
                                     $item['state_name'],
                                     $item['content']);
        }
        return $result;
    }
}