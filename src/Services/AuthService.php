<?php
/**
 * Creator:  Denis Golikov
 * Date: 07.03.2019
 * Time: 1:20
 */

namespace App\Services;

use App\Entities\User;
use App\Entities\UserKeys;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService
{
    /**
     * @param string $login
     * @param string $password
     * @return UserKeys
     * @throws \Exception
     */
    public function createKeys(string $login, string $password): UserKeys
    {
        $salt = bin2hex(random_bytes(30));
        $data = $login.$salt.$password;
        $password = hash('sha256', $data);
        $hash = bin2hex(random_bytes(30));
        $token = $this->createToken($login, $hash, $salt);

        return new UserKeys($login, $password, $salt, $hash, $token);
    }

    /**
     * @param User $user
     * @param string $password
     * @return bool
     */
    public function isValidPasswords(User $user, string $password): bool
    {
        $data = $user->getKeys()->getLogin().$user->getKeys()->getSalt().$password;
        $password = hash('sha256', $data);
        $correctPassword = $user->getKeys()->getPassword();
        return $password === $correctPassword;
    }

    /**
     * @param string $login
     * @param string $hash
     * @param string $salt
     * @return string
     */
    private function createToken(string $login, string $hash, string $salt): string
    {
        $hash = $login.$hash.$salt;
        return hash('sha256', $hash);
    }

}