<?php
/**
 * Creator:  Denis Golikov
 * Date: 03.03.2019
 * Time: 0:31
 */

namespace Core;

use App\Controllers\AController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException as RouteException;


/**
 * Class Kernel
 * @package Core
 */
class Kernel
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     * @return Response
     */
    public function execute(Request $request): Response
    {
        $this->request = $request;

        $context = new RequestContext();
        $routes = $this->getRoutes(Yaml::parseFile(ROUTES));

        $context->fromRequest($request);
        $matcher = new UrlMatcher($routes, $context);

        return $this->runController($matcher);

    }

    /**
     * @param UrlMatcher $matcher
     * @return Response
     */
    private function runController(UrlMatcher $matcher): Response
    {
        try {
            $routeData = $matcher->match($this->request->getRequestUri());
        } catch (RouteException $e) {
            return new Response('Resource not found', 404);
        }

        $controllerPath = $routeData['_controller'];
        $controllerMethod = $routeData['_action'];

        $controller = $this->getController($controllerPath);
        return $controller->{$controllerMethod}();
    }

    /**
     * @param string $controllerPath
     * @return AController
     */
    private function getController(string $controllerPath): AController
    {
        return new $controllerPath($this->request);
    }

    /**
     * @param array $routes
     * @return RouteCollection
     */
    private function getRoutes(array $routes): RouteCollection
    {
        $result = new RouteCollection();
        foreach ($routes as $name => $options) {
            $result->add($name, new Route(
                $options['path'],
                $options['default'],
                $options['requirements'],
                $options['options'],
                $options['host'],
                $options['schemes'],
                $options['method']
            ));
        }
        return $result;
    }
}